<?php

namespace Database\Seeders;

use App\Models\Award;
use App\Models\Point;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'Administrador Dalmobíle',
            'email' => 'admin@dalmobile.com.br',
            'phone' => '(99) 9 9999-999',
            'birth' => date('Y-m-d'),
            'role_id' => 1,
            'password' => Hash::make('LIFEword1243')
        ]);
        // User::factory()->create([
        //     'name' => 'Flávio Augusto',
        //     'email' => 'arquiteto@dalmobile.com.br',
        //     'phone' => '(99) 9 9999-999',
        //     'birth' => date('Y-m-d'),
        //     'role_id' => 2,
        //     'password' => Hash::make('LIFEword1243')
        // ]);
        // User::factory()->create([
        //     'name' => 'Jeandreo Furquim',
        //     'email' => 'jeandreofur@gmail.com',
        //     'phone' => '(99) 9 9999-999',
        //     'birth' => date('Y-m-d'),
        //     'role_id' => 2,
        //     'password' => Hash::make('LIFEword1243')
        // ]);
        // Award::create([
        //     'name' => 'Jantar no Comodoro',
        //     'description' => 'Procurando por uma experiência gastronômica deliciosa? Junte-se a nós na hamburgueria Comodoro! Prepare-se para saborear hambúrgueres suculentos, acompanhamentos irresistíveis e um ambiente acolhedor.',
        //     'points' => 370,
        //     'created_by' => 1,
        // ]);
        // Award::create([
        //     'name' => 'PIX SEXTOU BB!!!',
        //     'description' => 'Gostaria de receber um PIX e aproveitar uma grana extra nessa sexta? Temos uma ótima notícia para você!',
        //     'points' => 250,
        //     'created_by' => 1,
        // ]);
        // Award::create([
        //     'name' => 'Rodízio para o casal por nossa conta! <3',
        //     'description' => 'Procurando por uma experiência gastronômica única para você e seu(a) parceiro(a)? Não procure mais! Apresentamos nosso exclusivo rodízio para casal, uma delícia para ser compartilhada a dois.            ',
        //     'points' => 350,
        //     'created_by' => 1,
        // ]);

        // Point::create([
        //     'user_id' => 3,
        //     'title' => 'Pontos adicionados',
        //     'points' => rand(9000, 15000),
        //     'created_by' => 1,
        // ]);






        // \App\Models\User::factory(10)->create();
        // \App\Models\Point::factory(50)->create();
    }
}
