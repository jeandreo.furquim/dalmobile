<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('birth')->after('phone')->nullable();
            $table->string('cpf')->after('birth')->nullable();
            $table->string('street')->after('cpf')->nullable();
            $table->string('number')->after('street')->nullable();
            $table->string('complement')->after('number')->nullable();
            $table->string('neighborhood')->after('complement')->nullable();
            $table->string('city')->after('neighborhood')->nullable();
            $table->integer('state')->after('city')->nullable();
            $table->string('zip')->after('state')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['zip', 'state', 'city', 'neighborhood', 'complement', 'number', 'address', 'cpf', 'birth']);
        });
    }
};
