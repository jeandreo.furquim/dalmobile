// MASKS
Inputmask(["(99) 9999-9999", "(99) 9 9999-9999"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-phone");

Inputmask(["99.999.999/9999-99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-cnpj");

Inputmask(["999.999.999-99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-cpf");

Inputmask(["99999-999"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-cep");

Inputmask(["99/99/9999"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-date");

Inputmask(["99/99/9999 99:99:99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-date-time");

Inputmask(["99:99:99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-duration");

Inputmask(["99:99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-time");
// INPUT MASKS
Inputmask(["9999.99.99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-ncm");

Inputmask(["9.99", "99.99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-comission");

Inputmask(["9.999g", "99.999g", "999.999g"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-weight");

Inputmask(["9.9cm", "99.9cm", "999.9cm"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-cm");

Inputmask(["R$ 9", "R$ 99", "R$ 9,99", "R$ 99,99", "R$ 999,99", "R$ 9.999,99", "R$ 99.999,99", "R$ 999.999,99", "R$ 9.999.999,99"], {
    "numericInput": true,
    "clearIncomplete": true,
}).mask(".input-money");

// MARK ALL TRUE
$('.select-all').click(function(){
    $('.form-check-input').prop('checked', true);
});

// MARK ALL FALSE
$('.unselect-all').click(function(){
    $('.form-check-input').prop('checked', false);
});

// GENERATE TABLES
function loadTables(tableSelector = '#datatables', items = 25, order = [0, 'ASC']) {

    // SELECT TABLE
    var table = $(tableSelector);

    // CONFIG TABLE
    var dataTableOptions = {
        pageLength: items,
        order: order,
        language: {
            search: 'Pesquisar:',
            lengthMenu: 'Mostrando _MENU_ registros por página',
            zeroRecords: 'Ops, não encontramos nenhum resultado :(',
            info: 'Mostrando página _PAGE_ de _PAGES_',
            infoEmpty: 'Ops, nenhum registro disponível',
            infoFiltered: '(Filtrando _MAX_ registros)',
            processing: 'Filtrando dados',
            paginate: {
                previous: '<i class="ki-duotone ki-entrance-right page-left-right"><i class="path1"></i><i class="path2"></i></i>',
                next: '<i class="ki-duotone ki-entrance-left page-left-right"><i class="path1"></i><i class="path2"></i></i>',
            },
        },
        dom:
            "<'row'" +
            "<'col-sm-6 d-flex align-items-center justify-content-start'l>" +
            "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
            '>' +
            "<'table-responsive'tr>" +
            "<'row'" +
            "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
            "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
            '>',
        };

    // MAKE TABLE
    table.DataTable(dataTableOptions);
    
    $(tableSelector).DataTable()

}


function generatePass(trigger = '.generate-pass', paste = '.replace-pass'){

    $(trigger).click(function(){

        console.log('Gerar Senha');

        var chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ!@#$%^&*()+?><:{}[]";
        var passwordLength = 16;
        var password = "";

        for (var i = 0; i < passwordLength; i++) {
            var randomNumber = Math.floor(Math.random() * chars.length);
            password += chars.substring(randomNumber, randomNumber + 1);
        }

        $(paste).val(password);

    });

}

function convertDateFormat($date)
{
    $dateObj = date_create_from_format('d/m/Y', $date);
    return date_format($dateObj, 'Y-m-d');
}