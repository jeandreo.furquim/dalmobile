<!DOCTYPE html>
<html lang="pt-BR">
	<!--begin::Head-->
	<head>
        <base href="">
		<title>Registrar - Dalmóbile</title>
		<meta charset="utf-8" />
		<meta name="description" content="#" />
		<meta name="keywords" content="#" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="pt_BR" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="shortcut icon" href="{{ asset('assets/media/images/favicon.ico') }}" />
		<!--begin::Fonts-->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/custom.css') }} " rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		<link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}" >
    </head>
    <body id="kt_body" class="bg-body" style="background: linear-gradient(35deg, black, #292e38) !important;">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
				<!--begin::Content-->
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					<!--begin::Wrapper-->
					<div class="w-lg-1000px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                        <div class="w-100 d-flex align-items-center justify-content-center">
                            <img alt="Logo" src="{{ asset('assets/media/images/logo-black.svg') }}" class="h-50px mb-10" />
                        </div>
                        @if(session('message'))
                        <h5 class="modal-title text-center mt-8">Atenção: <span class="fw-bold fs-6" style="color: red;">{{ session('message')}}</span></h5>
                        @endif
						<!--begin::Form-->
						<form action="{{ route('register.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row gx-10 mb-5">
                                @include('pages._forms.user')
                                <div class="d-flex justify-content-end mt-5">
									
									<a href="{{ route('login') }}"  type="submit" class="btn btn-lg btn-light me-4">
										Acessar
									</a>
                                    <input type="submit" class="btn btn-primary btn-active-danger" value="Cadastrar"></input>
                                </div>
                            </div>
                        </form>
						<!--end::Form-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Authentication - Sign-in-->
		</div>
        <!--end::Main-->
        <script>var hostUrl = "assets/";</script>
		<style>
            body {
                font-family: 'Jost', sans-serif;
            }
        </style>
        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src={{asset('assets/plugins/global/plugins.bundle.js')}}></script>
        <script src={{asset('assets/js/scripts.bundle.js')}}></script>
		<script src="{{ asset('assets/js/custom.js') }} "></script>
		@yield('custom-footer');
        <!--end::Global Javascript Bundle-->
        <!--begin::Page Custom Javascript(used by this page)-->
        <script src={{asset('assets/js/custom/authentication/sign-in/general.js')}}></script>
        <!--end::Page Custom Javascript-->
        <!--end::Javascript-->
    </body>
    <!--end::Body-->
</html>
