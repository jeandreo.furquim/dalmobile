<!DOCTYPE html>
<html lang="pt-BR">
	<!--begin::Head-->
	<head>
        <base href="">
		<title>Recuperar Senha - Dalmóbile</title>
		<meta charset="utf-8" />
		<meta name="description" content="#" />
		<meta name="keywords" content="#" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="pt_BR" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="shortcut icon" href="{{ asset('assets/media/images/favicon.ico') }}" />
		<!--begin::Fonts-->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		<link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}" >
    </head>
    <body id="kt_body" class="bg-body" style="background: linear-gradient(35deg, black, #292e38) !important;">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
				<!--begin::Content-->
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					<!--begin::Wrapper-->
					<div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                        <div class="w-100 d-flex align-items-center justify-content-center">
                            <img alt="Logo" src="{{ asset('assets/media/images/logo-black.svg') }}" class="h-50px mb-10" />
                        </div>
                        <p class="text-gray-700 mb-5">Esqueceu sua senha? Sem problemas. Basta nos informar seu endereço de e-mail e enviaremos por e-mail um link de redefinição de senha que permitirá que você escolha uma nova.</p>
                        <x-auth-session-status class="mb-4 text-danger fw-bolder" :status="session('status')" />
						<!--begin::Form-->
                        <form class="form w-100" method="POST" action="{{ route('password.email') }}">
                            @csrf
                    
                            <!-- Email Address -->
                            <div>
                                <label class="form-label fs-6 fw-bolder text-dark">E-mail</label>
                                <input id="email" class="form-control form-control-lg form-control-solid" type="email" name="email" :value="old('email')" required autofocus />
                            </div>
                    
                            <div class="flex items-center justify-end mt-4">
                                <button type="submit" class="btn btn-lg btn-primary w-100 mb-5">
                                    <span class="indicator-label">{{ __('Recuperar Senha') }}</span>
                                </button>
                            </div>
                        </form>
						<!--end::Form-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Authentication - Sign-in-->
		</div>
        <!--end::Main-->
        <style>
            body {
                font-family: 'Jost', sans-serif;
            }
        </style>
        <script>var hostUrl = "assets/";</script>
        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src={{asset('assets/plugins/global/plugins.bundle.js')}}></script>
        <script src={{asset('assets/js/scripts.bundle.js')}}></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Page Custom Javascript(used by this page)-->
        <script src={{asset('assets/js/custom/authentication/sign-in/general.js')}}></script>
        <!--end::Page Custom Javascript-->
        <!--end::Javascript-->
    </body>
    <!--end::Body-->
</html>