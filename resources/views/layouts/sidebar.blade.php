<div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="225px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">
    <!--begin::Logo-->
    <div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">
        <!--begin::Logo image-->
        <a href="{{ route('dashboard') }}">
            <img alt="Logo" src="{{ asset('assets/media/images/logo-white.svg') }}" class="h-25px app-sidebar-logo-default" />
            <img alt="Logo" src="{{ asset('assets/media/images/favicon.ico') }}" class="h-20px app-sidebar-logo-minimize" />
        </a>
        <div id="kt_app_sidebar_toggle" class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary body-bg h-30px w-30px position-absolute top-50 start-100 translate-middle rotate" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="app-sidebar-minimize">
            <i class="ki-duotone ki-double-left fs-2 rotate-180">
                <span class="path1"></span>
                <span class="path2"></span>
            </i>
        </div>
        <!--end::Sidebar toggle-->
    </div>
    <!--end::Logo-->
    <!--begin::sidebar menu-->
    <div class="app-sidebar-menu overflow-hidden flex-column-fluid">
        <!--begin::Menu wrapper-->
        <div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper hover-scroll-overlay-y my-5" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer" data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true">
            <!--begin::Menu-->
            <div class="menu menu-column menu-rounded menu-sub-indention px-3" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false">
                <!--begin:Menu item-->
                <div class="menu-item pt-5">
                    <!--begin:Menu content-->
                    <div class="menu-content">
                        <span class="menu-heading fw-bold text-uppercase fs-7">Menu</span>
                    </div>
                    <!--end:Menu content-->
                </div>
                <!--end:Menu item-->
                @if(Auth::user()->role_id == 1)
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('dashboard') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-chart-simple fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Dashboard</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('users.index', 2) }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-users fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Arquitetos</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('users.index', 3) }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-users fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Decoradores</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('users.birthdays') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-cake-candles fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Aniversariantes</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('points.index') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-coins fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Pontos</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('users.ranking') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-ranking-star fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Ranking</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('awards.index') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-trophy fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Prêmios</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('send.awards.index') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-gift fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Envios de prêmios</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                @else
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('dashboard') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-chart-simple fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Início</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('points.index') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-ranking-star fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Pontos</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('awards.results') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-trophy fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Prêmios</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ url('assets/files/termo.pdf') }}" target="_blank">
                        <span class="menu-icon">
                            <i class="fa-solid fa-file-lines fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Termos de uso</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="{{ route('points.table') }}">
                        <span class="menu-icon">
                            <i class="fa-solid fa-star fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Regra de Pontuação</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="#">
                        <span class="menu-icon">
                            <i class="fa-brands fa-whatsapp fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Nosso Whatsapp</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <!--begin:Menu item-->
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link" href="#">
                        <span class="menu-icon">
                            <i class="fa-solid fa-globe fs-5"></i>
                        </span>
                        <span class="menu-title fs-5 ms-2">Nosso Site</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <!--end:Menu item-->
                <p class="m-0 text-center fw-bolder mt-5"><span class="badge badge-primary fs-6">SEUS PONTOS: {{ Auth::user()->points->sum('points')}}</span></p>
                @endif
            </div>
            <!--end::Menu-->
        </div>
        <!--end::Menu wrapper-->
    </div>
    <!--end::sidebar menu-->
    <!--begin::Footer-->
    <div class="app-sidebar-footer flex-column-auto pt-2 pb-6 px-6" id="kt_app_sidebar_footer">
        <a href="https://dalmobilesjc.com.br/" target="_blank" class="btn btn-flex btn-sidebar flex-center btn-custom overflow-hidden text-nowrap px-0 h-40px w-100">
            <span class="btn-label">www.dalmobilesjc.com.br</span>
            <i class="fa-solid fa-globe btn-icon fs-5 ms-1"></i>
        </a>
    </div>
    <!--end::Footer-->
</div>