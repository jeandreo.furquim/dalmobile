<div id="kt_app_footer" class="app-footer">
    <!--begin::Footer container-->
    <div class="app-container container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted fw-semibold me-1">{{ date('Y') }}&copy;</span>
            <a href="https://dalmobilesjc.com.br/" target="_blank" class="text-gray-800 text-hover-primary">Dalmóbile Points - Todos os direitos reservados</a>
        </div>
        <!--end::Copyright-->
        <!--begin::Menu-->
        <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
            <li class="menu-item">
                <a href="https://api.whatsapp.com/send?phone=5512996049888&text=Ol%C3%A1,%20gostaria%20de%20fala%20com%20o%20suporte%20do%20sistema%20Dalm%C3%B3bile." target="_blank" class="menu-link px-2">Suporte Técnico</a>
            </li>
        </ul>
        <!--end::Menu-->
    </div>
    <!--end::Footer container-->
</div>