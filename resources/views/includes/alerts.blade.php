@if(session('message'))
    <button type="button" class="d-none" id="btn-alert" data-bs-toggle="modal" data-bs-target="#modal_alerts"></button>
    <div class="modal fade" tabindex="-1" id="modal_alerts">
        <div class="modal-dialog h-100 d-flex align-items-center mx-auto m-0">
            <div class="modal-content">
                <div class="modal-header" style="border: none !important">
                    <span class="text-muted fw-bold fs-4 alert-custom">{!! session('message') !!}</span>
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-light ms-4" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </div>
                    <!--end::Close-->
                </div>
            </div>
        </div>
    </div>
@endif