@extends('layouts.app')

@section('page-title', 'Prêmios')

@section('url-toolbar', route('awards.create'))

@section('text-toolbar', 'Adicionar Prêmio')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                <div class="col-xl-12 p-0">
                    <div class="card">
                        <div class="card-body fs-6 p-10 text-gray-700">
                            <table id="datatables" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                                <thead>
                                    <tr class="fw-bold fs-6 text-gray-800 px-7">
                                        <th class="w-50px">ID</th>
                                        <th>Nome</th>
                                        <th>Pontos</th>
                                        <th>Válido</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody class="table-pd">
                                    @foreach ($contents as $content)
                                    <tr>
                                        <td>
                                            <span class="text-gray-700">{{ $content->id }}</span>
                                        </td>
                                        <td style="min-width: 25%; padding: 11px;">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-30px me-5">
                                                    <img src="{{ searchImage('premios', $content->id) }}" class="rounded object-fit-cover" alt="">
                                                </div>
                                                <div class="d-flex justify-content-start">
                                                    <a href="{{ route('awards.edit', $content->id) }}" class="text-gray-700 fw-bold text-hover-primary fs-6">{{ Str::limit($content->name, 35) }}</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-gray-700">
                                            <span class="badge badge-light-danger">{{ $content->points }}</span>
                                        </td>
                                        <td class="text-gray-700">
                                            <div class="d-flex align-items-center">
                                                @if($content->valid != null)
                                                <span class="badge badge-light-primary">{{ date('d/m/Y', strtotime($content->valid)) }}</span>
                                                @else
                                                <span class="badge badge-light">Sem validade</span>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-gray-700">
                                            <div class="d-flex align-items-center">
                                                @if($content->status == 1)
                                                <span class="badge badge-light-primary">Ativo</span>
                                                @else
                                                <span class="badge badge-light-danger">Inativo</span>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center icons-edit">
                                                <a href="{{ route('awards.edit', $content->id) }}">
                                                    <i class="fas fa-edit px-1" title="Editar"></i>
                                                </a>
                                                @if($content->status == 1)
                                                <a href="{{ route('awards.destroy', $content->id) }}">
                                                    <i class="fas fa-times-circle px-1" title="Desativar"></i>
                                                </a>
                                                @else
                                                <a href="{{ route('awards.destroy', $content->id) }}">
                                                    <i class="fas fa-redo px-1" title="Reativar"></i>
                                                </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<!--end::Content-->
@endsection