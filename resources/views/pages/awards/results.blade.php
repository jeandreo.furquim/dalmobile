@extends('layouts.app')

@section('page-title', 'Prêmios')

@section('url-toolbar', route('dashboard'))

@section('text-toolbar', 'Solicitações')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                @foreach ($contents as $content)
                <div class="col-md-3">
                    <div class="card h-100">
                        <div class="card-body p-2">
                            <img src="{{ searchImage('premios', $content->id, true, true) }}" class="rounded object-fit-cover w-100 h-150px" alt="">
                            <p class="text-center mb-0 mt-4 fs-4 text-gray-800 fw-bold">{{ $content->name }}</p>
                            <p class="text-center mt-2 mb-0">
                                <span class="badge badge-danger">Pontos: {{ $content->points }}</span>
                                @if($content->valid != null)
                                <span class="badge badge-primary">Válido até: {{ date('d/m/Y', strtotime($content->valid)) }}</span>
                                @endif
                            </p>
                        </div>
                        <div class="card-footer p-4">
                            @if (Auth::user()->points->sum('points') >= $content->points)
                            <a href="{{ route('send.awards.request', $content->id) }}" class="btn btn-sm btn-primary w-100 fs-6">Resgatar Prêmio</a>
                            @else
                            <span class="btn btn-sm btn-light cursor-default w-100 fs-6">Ganhe mais pontos</span>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<!--end::Content-->
@endsection