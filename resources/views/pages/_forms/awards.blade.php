<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Nome</label>
    <div class="mb-5">
        <input type="text" class="form-control form-control-solid" placeholder="Nome do Prêmio" name="name" value="{{ $content->name ?? old('name') }}" maxlength="255" required>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Custo em Pontos</label>
    <div class="mb-5">
        <input type="number" class="form-control form-control-solid" placeholder="Ex.: 500pts" name="points" value="{{ $content->points ?? old('points') }}" maxlength="255" required>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3">Validade</label>
    <div class="mb-5">
        <input type="text" class="form-control form-control-solid input-date" placeholder="00/00/0000" name="valid" value="@if(isset($content) && $content->valid != null) {{ date('d/m/Y', strtotime($content->valid)) }} @else{{ old('valid') }}@endif" maxlength="255">
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-12">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3">Descrição</label>
    <div class="mb-5">
        <textarea class="form-control form-control-solid" placeholder="Descreva sobre o prêmio aqui" name="description" value="" maxlength="1000">{{ $content->description ?? old('description') }}</textarea>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-12">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Imagem do Prêmio:</label>
    <input class="form-control form-control-solid image-to-crop" type="file" name="image" accept="image/*">
    <input type="hidden" value="" name="image">
</div>
<!--end::Col-->