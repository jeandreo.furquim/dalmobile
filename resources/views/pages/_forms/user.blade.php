<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Nome completo</label>
    <div class="mb-5">
        <input type="text" class="form-control form-control-solid" placeholder="Nome completo" name="name" value="{{ $content->name ?? old('name') }}" maxlength="255" required>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Nascimento</label>
    <div class="mb-5">
        <input type="text" class="form-control form-control-solid input-date" placeholder="00/00/0000" name="birth" value="@if(isset($content->birth)){{ date('d/m/Y', strtotime($content->birth)) }}@else{{ old('birth') }}@endif" maxlength="255" required>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Telefone</label>
    <div class="mb-5">
        <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="phone" value="{{ $content->phone ?? old('phone') }}" maxlength="255" required>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">CPF</label>
    <div class="mb-5">
        <input type="text" class="form-control form-control-solid input-cpf" placeholder="000.000.000-00" name="cpf" value="{{ $content->cpf ?? old('cpf') }}" maxlength="255" required>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Grupo de usuários</label>
    <div class="mb-5">
        <select name="role_id" class="form-select form-select-solid" data-control="select2" data-placeholder="Selecione uma opção" required>
            <option></option>
            @if(Auth::check() && Auth::user()->role_id == 1)
            <option value="1" @if(isset($content) && $content->role_id == 1) selected @endif>Administrador</option>
            @endif
            <option value="2" @if(isset($content) && $content->role_id == 2) selected @endif>Arquiteto</option>
            <option value="3" @if(isset($content) && $content->role_id == 3) selected @endif>Decorador</option>
        </select>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">E-mail:</label>
    <div class="mb-5">
        <input type="email" class="form-control form-control-solid" placeholder="email@gmail.com.br" name="email" value="{{ $content->email ?? old('email') }}" maxlength="255" required>
    </div>
    <!--end::Input group-->
</div>
<!--end::Col-->
<div class="col-3 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">CEP</label>
    <input class="form-control form-control-solid input-cep" placeholder="CEP" id="cep" type="text" name="zip" value="{{ $content->zip ?? old('zip') }}" required>
</div>
<div class="col-6 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Endereço</label>
    <input class="form-control form-control-solid" placeholder="Rua, Avenida..." type="text" name="street" value="{{ $content->street ?? old('street') }}" required>
</div>
<div class="col-3 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Número</label>
    <input class="form-control form-control-solid" placeholder="Número" type="text" name="number" value="{{ $content->number ?? old('number') }}" required>
</div>
<div class="col-3 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3">Complemento</label>
    <input class="form-control form-control-solid" placeholder="Complemento" type="text" name="complement" value="{{ $content->complement ?? old('complement') }}">
</div>
<div class="col-3 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Bairro</label>
    <input class="form-control form-control-solid" placeholder="Bairro" type="text" name="neighborhood" value="{{ $content->neighborhood ?? old('neighborhood') }}" required>
</div>
<div class="col-3 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Cidade</label>
    <input class="form-control form-control-solid" placeholder="Cidade" type="text" name="city" value="{{ $content->city ?? old('city') }}" required>
</div>
<div class="col-3 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Estado</label>
    <select class="form-select form-select-solid" name="state" data-control="select2" data-placeholder="Estado" required>
        <option></option>
        <option value="1" @if(isset($content->state) && $content->state == 1) selected @endif>Acre</option>
        <option value="2" @if(isset($content->state) && $content->state == 2) selected @endif>Alagoas</option>
        <option value="3" @if(isset($content->state) && $content->state == 3) selected @endif>Amapá</option>
        <option value="4" @if(isset($content->state) && $content->state == 4) selected @endif>Amazonas</option>
        <option value="5" @if(isset($content->state) && $content->state == 5) selected @endif>Bahia</option>
        <option value="6" @if(isset($content->state) && $content->state == 6) selected @endif>Ceará</option>
        <option value="7" @if(isset($content->state) && $content->state == 7) selected @endif>Distrito Federal</option>
        <option value="8" @if(isset($content->state) && $content->state == 8) selected @endif>Espírito Santo</option>
        <option value="9" @if(isset($content->state) && $content->state == 9) selected @endif>Espírito Santo</option>
        <option value="10" @if(isset($content->state) && $content->state == 10) selected @endif>Maranhão</option>
        <option value="11" @if(isset($content->state) && $content->state == 11) selected @endif>Mato Grosso</option>
        <option value="12" @if(isset($content->state) && $content->state == 12) selected @endif>Mato Grosso do Sul</option>
        <option value="13" @if(isset($content->state) && $content->state == 13) selected @endif>Minas Gerais</option>
        <option value="14" @if(isset($content->state) && $content->state == 14) selected @endif>Pará</option>
        <option value="15" @if(isset($content->state) && $content->state == 15) selected @endif>Paraíba</option>
        <option value="16" @if(isset($content->state) && $content->state == 16) selected @endif>Paraná</option>
        <option value="17" @if(isset($content->state) && $content->state == 17) selected @endif>Pernambuco</option>
        <option value="18" @if(isset($content->state) && $content->state == 18) selected @endif>Piauí</option>
        <option value="19" @if(isset($content->state) && $content->state == 19) selected @endif>Rio de Janeiro</option>
        <option value="20" @if(isset($content->state) && $content->state == 20) selected @endif>Rio Grande do Norte</option>
        <option value="21" @if(isset($content->state) && $content->state == 21) selected @endif>Rio Grande do Sul</option>
        <option value="22" @if(isset($content->state) && $content->state == 22) selected @endif>Rondônia</option>
        <option value="23" @if(isset($content->state) && $content->state == 23) selected @endif>Roraima</option>
        <option value="24" @if(isset($content->state) && $content->state == 24) selected @endif>Santa Catarina</option>
        <option value="25" @if(isset($content->state) && $content->state == 25) selected @endif>São Paulo</option>
        <option value="26" @if(isset($content->state) && $content->state == 26) selected @endif>Sergipe</option>
        <option value="27" @if(isset($content->state) && $content->state == 27) selected @endif>Tocantins</option>
    </select>
</div>
<!--begin::Col-->
<div class="col-lg-5 mb-5">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Imagem do Usuário:</label>
    <input class="form-control form-control-solid image-to-crop" type="file" name="image" accept="image/*">
    <input type="hidden" value="" name="image">
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-4">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 @if(!isset($content)) required @endif">Senha</label>
    <!--begin::Main wrapper-->
    <div class="fv-row" data-kt-password-meter="true">
        <!--begin::Wrapper-->
        <div class="mb-1">
            <!--begin::Input wrapper-->
            <div class="position-relative mb-3">
                <input class="form-control form-control-lg form-control-solid replace-pass" type="password" name="password" placeholder="********" @if(!isset($content)) required @endif/>
                <!--begin::Visibility toggle-->
                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                    <i class="bi bi-eye-slash fs-2"></i>
                    <i class="bi bi-eye fs-2 d-none"></i>
                </span>
                <!--end::Visibility toggle-->
            </div>
            <!--end::Input wrapper-->
        </div>
    </div>
    <!--end::Main wrapper-->
</div>
<div class="col-lg-3">
    <div class="form-label fs-6 fw-bold mb-3 text-white">-</div>
    <span class="btn btn-primary d-flex align-items-center justify-content-center generate-pass">
        <i class="fa-solid fa-rotate text-white"></i>
        Gerar senha
    </span>
    <!--end::Main wrapper-->
</div>
<!--end::Col-->

@section('custom-footer')
<script>
    generatePass('.generate-pass', '.replace-pass');
</script>
@endsection