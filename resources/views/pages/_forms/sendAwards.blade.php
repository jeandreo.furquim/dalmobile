@if (!isset($content))
    <!--begin::Col-->
    <div class="col-lg-12">
        <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Prêmio</label>
        <div class="mb-5">
            <select class="form-select form-select-solid" name="award_id" data-control="select2" data-placeholder="Selecione um Prêmio" required>
                <option></option>
                @foreach ($awards as $award)
                <option value="{{ $award->id }}" @if(isset($content) && $award->id == $content->user) selected @endif>{{ $award->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-lg-12">
        <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Arquiteto</label>
        <div class="mb-5">
            <select class="form-select form-select-solid" name="user_id" data-control="select2" data-placeholder="Selecione um arquiteto" required>
                <option></option>
                @foreach ($users as $user)
                <option value="{{ $user->id }}" @if(isset($content) && $user->id == $content->user) selected @endif>{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!--end::Col-->
@endif
<!--begin::Col-->
<div class="col-lg-12">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3">Mensagem amigável:</label>
    <div class="mb-5">
        <textarea class="form-control form-control-solid" placeholder="Envie junto ao prêmio, uma mensagem positiva pra seu colaborador." name="observation" value="" maxlength="1000">{{ $content->observation ?? old('observation') }}</textarea>
    </div>
</div>
<!--end::Col-->