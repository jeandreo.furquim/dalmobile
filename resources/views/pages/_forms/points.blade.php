<!--begin::Col-->
<div class="col-lg-12">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Arquiteto</label>
    <div class="mb-5">
        <select class="form-select form-select-solid" name="user_id" data-control="select2" data-placeholder="Selecione um arquiteto" required>
            <option></option>
            @foreach ($users as $user)
            <option value="{{ $user->id }}" @if(isset($content) && $user->id == $content->user) selected @endif>{{ $user->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-12">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Deseja adicionar ou remover pontos?</label>
    <div class="mb-5">
        <select class="form-select form-select-solid plus-minus" name="addOrRemove" data-control="select2" data-placeholder="Selecione uma opção" required>
            <option></option>
            <option value="1">Adicionar</option>
            <option value="2">Remover</option>
        </select>
    </div>
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-lg-12">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Quantidade:</label>
    <div class="mb-5">
        <div class="row">
            <div class="col-1">
                <div class="h-100 w-100 d-flex align-items-center justify-content-center">
                    <i class="fa-solid fa-circle-plus text-success fs-1 icon-points"></i>
                </div>
            </div>
            <div class="col">
                <input type="number" class="form-control form-control-solid" placeholder="Ex.: 500pts" name="points" value="{{ $content->points ?? old('points') }}" maxlength="255" required>
            </div>
        </div>
    </div>
</div>
<!--end::Col-->


<!--begin::Col-->
<div class="col-lg-12">
    <label class="form-label fs-6 fw-bold text-gray-700 mb-3 required">Contrato:</label>
    <div class="mb-5">
        <div class="row">
            <div class="col">
                <input type="text" class="form-control form-control-solid" placeholder="Contrato" name="contract" value="{{ $content->contract ?? old('contract') }}" maxlength="255" required>
            </div>
        </div>
    </div>
</div>
<!--end::Col-->

@section('custom-footer')
<script>

    $('.plus-minus').change(function(){
        var addOrRemove = $(this).val();
        console.log(addOrRemove);

        if (addOrRemove == 1) {
            $('.icon-points').addClass('fa-circle-plus text-success').removeClass('fa-circle-minus text-danger');
        } else {
            $('.icon-points').addClass('fa-circle-minus text-danger').removeClass('fa-circle-plus text-success');
        }
    });

</script>
@endsection