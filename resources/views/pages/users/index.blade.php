@extends('layouts.app')

@section('page-title', 'Usuários')

@section('url-toolbar', route('users.create'))

@section('text-toolbar', 'Adicionar Usuário')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                <div class="col-xl-12 p-0">
                    <div class="card">
                        <div class="card-body fs-6 p-10 text-gray-700">
                            <table id="datatables" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                                <thead>
                                    <tr class="fw-bold fs-6 text-gray-800 px-7">
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Tipo</th>
                                        <th>Pontos</th>
                                        <th>Telefone</th>
                                        <th>E-mail</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody class="table-pd">
                                    @foreach ($contents as $content)
                                    <tr>
                                        <td>
                                            <span class="text-gray-700">{{ $content->id }}</span>
                                        </td>
                                        <td style="min-width: 25%; padding: 11px;">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-30px me-5">
                                                    <img src="{{ searchImage('usuarios', $content->id, true, true) }}" class="rounded object-fit-cover" alt="">
                                                </div>
                                                <div class="d-flex justify-content-start">
                                                    <a href="{{ route('users.edit', $content->id) }}" class="text-gray-700 fw-bold text-hover-primary fs-6">{{ Str::limit($content->name, 20) }}</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            @if ($content->role_id == 1)
                                                <span class="badge badge-light-danger">Administrador</span>
                                            @elseif($content->role_id == 2)
                                                <span class="badge badge-light-info">Arquiteto</span>
                                            @else
                                                <span class="badge badge-light-primary">Decorador</span>
                                            @endif
                                        </td>
                                        <td class="text-gray-700">
                                            @if($content->points->sum('points') != 0)
                                            <span class="badge @if($content->points->sum('points') < 0) badge-light-danger @else badge-light-success @endif">
                                                {{ $content->points->sum('points') }}
                                            </span>
                                            @else
                                            <span class="badge badge-light">
                                               Sem pontos
                                            </span>
                                            @endif
                                        </td>
                                        <td class="text-gray-700">
                                            @if ($content->phone != null)
                                                <span class="badge badge-light-primary">{{ $content->phone }}</span>
                                            @else
                                                <span class="badge badge-light">Sem telefone</span>
                                            @endif
                                        </td>
                                        <td class="text-gray-700">
                                            <div class="d-flex align-items-center">
                                                {{ $content->email }}
                                            </div>
                                        </td>
                                        <td class="text-gray-700">
                                            <div class="d-flex align-items-center">
                                                @if($content->status == 1)
                                                <span class="badge badge-light-primary">Ativo</span>
                                                @elseif($content->status == 2)
                                                <span class="badge badge-light-warning">Pendente</span>
                                                @else
                                                <span class="badge badge-light-danger">Inativo</span>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center icons-edit">
                                                <a href="{{ route('users.edit', $content->id) }}">
                                                    <i class="fas fa-edit ps-1" title="Editar"></i>
                                                </a>
                                                @if($content->status == 2)
                                                <a href="{{ route('users.aprove', ['id' => $content->id, 'status' => 1]) }}">
                                                    <i class="fa-solid fa-circle-check ps-1" title="Aprovar"></i>
                                                </a>
                                                @endif
                                                @if($content->status == 2)
                                                <a href="{{ route('users.aprove', ['id' => $content->id, 'status' => 0]) }}">
                                                    <i class="fa-solid fa-circle-xmark ps-1" title="Recusar"></i>
                                                </a>
                                                @endif
                                                @if($content->status == 1)
                                                <a href="{{ route('users.destroy', $content->id) }}">
                                                    <i class="fas fa-times-circle ps-1" title="Desativar"></i>
                                                </a>
                                                @else
                                                <a href="{{ route('users.destroy', $content->id) }}">
                                                    <i class="fas fa-redo ps-1" title="Reativar"></i>
                                                </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<!--end::Content-->
@endsection