@extends('layouts.app')

@section('page-title', 'Minha Conta')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid pt-10">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                <div class="col-xl-12 m-0 p-0">
                    <div class="card">
                        <div class="card-body fs-6 p-10 text-gray-700">
                            <form action="{{ route('users.accountUp') }}" method="POST" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="row gx-10 mb-5">
                                    @include('pages._forms.user')
                                    <div class="d-flex justify-content-between mt-5">
                                        <div>
                                            <a href="{{ route('users.index') }}">
                                                <label class="btn btn-light text-muted ml-2">Voltar</label>
                                            </a>
                                        </div>
                                        <div>
                                            <input type="submit" class="btn btn-primary btn-active-danger text-light mb-3" value="Atualizar"></input>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
</div>
<!--end::Content-->
@endsection