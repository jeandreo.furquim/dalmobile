@extends('layouts.app')

@section('page-title', 'Gerenciar Pontos')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid pt-10">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                @if (Auth::user()->role_id == 1)
                <div class="col-4">
                </div>
                <div class="col-xl-4 m-0 p-0">
                    <div class="card">
                        <div class="card-body fs-6 p-10 text-gray-700">
                            <form action="{{ route('points.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row gx-10 mb-5">
                                    @include('pages._forms.points')
                                    <div class="d-flex justify-content-between mt-5">
                                        <div>
                                            <a href="{{ route('points.index') }}">
                                                <label class="btn btn-light text-muted ml-2">Voltar</label>
                                            </a>
                                        </div>
                                        <div>
                                            <input type="submit" class="btn btn-primary btn-active-danger mb-3" value="Cadastrar"></input>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @else
                <div class="card mt-0">
                    <div class="card-body">
                        <table id="datatables" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                            <thead>
                                <tr class="fw-bold fs-6 text-gray-800 px-7">
                                    <th class="w-10px pe-2">ID</th>
                                    <th>Ação</th>
                                    <th>Pontos</th>
                                    <th>Quando</th>
                                    <th>Por</th>
                                </tr>
                            </thead>
                            <tbody class="table-pd">
                                @foreach (Auth::user()->points as $point)
                                <tr>
                                    <td>
                                        <span class="text-gray-700">{{ $point->id }}</span>
                                    </td>
                                    <td>
                                        <span class="text-gray-700 fw-bold text-hover-primary fs-6">{{ $point->title }}</span>
                                    </td>
                                    <td class="text-gray-700">
                                        <span class="badge @if($point->points < 0) badge-light-danger @else badge-light-success @endif">
                                            {{ $point->points }}
                                        </span>
                                    </td>
                                    <td class="text-gray-700">
                                        <span class="badge badge-light">{{ $point->created_at->format('d/m/Y H:i:s') }}</span>
                                    </td>
                                    <td class="text-gray-700">
                                        <div class="d-flex align-items-center">
                                            {{ $point->author->name }}
                                        </div>
                                    </td>
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
</div>
<!--end::Content-->
@endsection