@extends('layouts.app')

@section('page-title', 'Pontuação')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid pt-10">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                <div class="col-2"></div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="text-center m-0 fw-bolder">VENDA A VISTA - ATÉ 50 KM DA CEDE</h2>
                            <p class="m-0 fs-4 text-center text-gray-600 mt-1 mb-4">A cada R$ 100,00 são <b>100</b> pontos acumuados</p>
                            <div class="table-responsive">
                                <table class="table table-striped gy-3 gs-3">
                                     <thead>
                                        <tr class="fw-semibold fs-6 text-gray-800 border-bottom border-gray-200">
                                            <th>Parcelas</th>
                                            <th>A cada</th>
                                            <th>Pontos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>ATÉ 3x</td>
                                            <td>R$ 100,00</td>
                                            <td>100 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>4x</td>
                                            <td>R$ 100,00</td>
                                            <td>93 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>5x</td>
                                            <td>R$ 100,00</td>
                                            <td>92 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>6x</td>
                                            <td>R$ 100,00</td>
                                            <td>91 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>7x</td>
                                            <td>R$ 100,00</td>
                                            <td>90 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>8x</td>
                                            <td>R$ 100,00</td>
                                            <td>89 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>9x</td>
                                            <td>R$ 100,00</td>
                                            <td>88 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>10x</td>
                                            <td>R$ 100,00</td>
                                            <td>87 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>11x</td>
                                            <td>R$ 100,00</td>
                                            <td>86 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>12x</td>
                                            <td>R$ 100,00</td>
                                            <td>85 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>13x</td>
                                            <td>R$ 100,00</td>
                                            <td>84 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>14x</td>
                                            <td>R$ 100,00</td>
                                            <td>83 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>15x</td>
                                            <td>R$ 100,00</td>
                                            <td>82 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>16x</td>
                                            <td>R$ 100,00</td>
                                            <td>81 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>17x</td>
                                            <td>R$ 100,00</td>
                                            <td>80 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>18x</td>
                                            <td>R$ 100,00</td>
                                            <td>79 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>19x</td>
                                            <td>R$ 100,00</td>
                                            <td>78 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>20x</td>
                                            <td>R$ 100,00</td>
                                            <td>77 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>21x</td>
                                            <td>R$ 100,00</td>
                                            <td>76 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>22x</td>
                                            <td>R$ 100,00</td>
                                            <td>75 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>23x</td>
                                            <td>R$ 100,00</td>
                                            <td>74 PONTOS</td>
                                        <tr>
                                        <tr>
                                            <td>24x</td>
                                            <td>R$ 100,00</td>
                                            <td>73 PONTOS</td>
                                        <tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="text-center m-0 fw-bolder">VENDA A VISTA - ACIMA DE 50 KM DA CEDE</h2>
                            <p class="m-0 fs-4 text-center text-gray-600 mt-1 mb-4">A cada R$ 100,00 são <b>93</b> pontos acumuados</p>
                            <div class="table-responsive">
                                <table class="table table-striped gy-3 gs-3">
                                    <thead>
                                        <tr class="fw-semibold fs-6 text-gray-800 border-bottom border-gray-200">
                                            <th>Parcelas</th>
                                            <th>A cada</th>
                                            <th>Pontos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>ATÉ 3x</td>
                                            <td>R$ 100,00</td>
                                            <td>93 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>4x</td>
                                            <td>R$ 100,00</td>
                                            <td>86 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>5x</td>
                                            <td>R$ 100,00</td>
                                            <td>85 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>6x</td>
                                            <td>R$ 100,00</td>
                                            <td>84 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>7x</td>
                                            <td>R$ 100,00</td>
                                            <td>83 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>8x</td>
                                            <td>R$ 100,00</td>
                                            <td>82 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>9x</td>
                                            <td>R$ 100,00</td>
                                            <td>81 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>10x</td>
                                            <td>R$ 100,00</td>
                                            <td>80 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>11x</td>
                                            <td>R$ 100,00</td>
                                            <td>79 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>12x</td>
                                            <td>R$ 100,00</td>
                                            <td>78 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>13x</td>
                                            <td>R$ 100,00</td>
                                            <td>77 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>14x</td>
                                            <td>R$ 100,00</td>
                                            <td>76 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>15x</td>
                                            <td>R$ 100,00</td>
                                            <td>75 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>16x</td>
                                            <td>R$ 100,00</td>
                                            <td>74 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>17x</td>
                                            <td>R$ 100,00</td>
                                            <td>73 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>18x</td>
                                            <td>R$ 100,00</td>
                                            <td>72 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>19x</td>
                                            <td>R$ 100,00</td>
                                            <td>71 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>20x</td>
                                            <td>R$ 100,00</td>
                                            <td>70 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>21x</td>
                                            <td>R$ 100,00</td>
                                            <td>69 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>22x</td>
                                            <td>R$ 100,00</td>
                                            <td>68 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>23x</td>
                                            <td>R$ 100,00</td>
                                            <td>67 PONTOS</td>
                                        </tr>
                                        <tr>
                                            <td>24x</td>
                                            <td>R$ 100,00</td>
                                            <td>66 PONTOS</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
</div>
<!--end::Content-->
@endsection