@extends('layouts.app')

@section('page-title', 'Editar Mensagem Prêmio')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid pt-10">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                <div class="col-xl-12 m-0 p-0">
                    <div class="card">
                        <div class="card-body fs-6 p-10 text-gray-700">
                            <form action="{{ route('send.awards.update', $content->id) }}" method="POST" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="row gx-10 mb-5">
                                    @include('pages._forms.sendAwards')
                                    <div class="d-flex justify-content-end mt-5">
                                        <input type="submit" class="btn btn-primary btn-active-danger text-light mb-3" value="Atualizar"></input>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
</div>
<!--end::Content-->
@endsection


@section('custom-footer')

<script>

    $('.short-name').keyup(function(){
        var input = $(this);
        var words = input.val().split(' ');
        if (words.length > 2) {
            input.val(words.slice(0, 2).join(' '));
        }
        $('.input-email').val(input.val().normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/\s+/g, ".").toLowerCase() + '@dalmobilesjc.com.br');
    });

    $('#auto-pass').click(function(){
        var chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ!@#$%^&*()+?><:{}[]";
        var passwordLength = 16;
        var password = "";

        for (var i = 0; i < passwordLength; i++) {
            var randomNumber = Math.floor(Math.random() * chars.length);
            password += chars.substring(randomNumber, randomNumber + 1);
        }
        console.log(password);
        $('[name="password"]').val(password);
    });

</script>

@endsection