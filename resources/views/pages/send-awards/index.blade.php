@extends('layouts.app')

@section('page-title', 'Gerenciar Prêmios')

@section('url-toolbar', route('send.awards.create'))

@section('text-toolbar', 'Enviar Prêmio')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                <div class="col-xl-12 p-0">
                    <div class="card">
                        <div class="card-body fs-6 p-10 text-gray-700">
                            <table id="datatables" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                                <thead>
                                    <tr class="fw-bold fs-6 text-gray-800 px-7">
                                        <th class="w-50px">ID</th>
                                        <th>Enviado para</th>
                                        <th>Premio</th>
                                        <th>Enviado por</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody class="table-pd">
                                    @foreach ($contents as $content)
                                    <tr>
                                        <td>
                                            <span class="text-gray-700">{{ $content->id }}</span>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex justify-content-start">
                                                    <a href="{{ route('send.awards.edit', $content->id) }}" class="text-gray-700 fw-bold text-hover-primary fs-6">{{ Str::limit($content->sendFor->name, 30) }}</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-gray-700">
                                            <span class="badge badge-light-info">{{ $content->award->name }}</span>
                                        </td>
                                        <td>
                                            <span class="badge badge-light">{{ Str::limit($content->author->name, 20) }}</span>
                                        </td>
                                        <td class="text-gray-700">
                                            <div class="d-flex align-items-center">
                                                @if($content->status == 1)
                                                <span class="badge badge-light-success">Enviado</span>
                                                @elseif($content->status == 3)
                                                <span class="badge badge-light-danger">Cancelado</span>
                                                @else
                                                <span class="badge badge-light-warning">Solicitado</span>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center icons-edit">
                                                <a href="{{ route('send.awards.edit', $content->id) }}">
                                                    <i class="fas fa-edit px-1" title="Editar"></i>
                                                </a>
                                                @if($content->status == 2)
                                                <a href="{{ route('send.awards.destroy', ['id' => $content->id, 'status' => 1]) }}">
                                                    <i class="fa-solid fa-circle-check px-1" title="Aprovar"></i>
                                                </a>
                                                <a href="{{ route('send.awards.destroy', ['id' => $content->id, 'status' => 3]) }}">
                                                    <i class="fa-solid fa-circle-xmark px-1" title="Cancelar"></i>
                                                </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<!--end::Content-->
@endsection