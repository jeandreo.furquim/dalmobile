@extends('layouts.app')

@section('page-title', 'Enviar Prêmio')

@section('content')
<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid pt-10">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Row-->
            <div class="row g-5 g-xl-8">
                <div class="col-xl-12 m-0 p-0">
                    <div class="card">
                        <div class="card-body fs-6 p-10 text-gray-700">
                            <form action="{{ route('send.awards.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row gx-10 mb-5">
                                    @include('pages._forms.sendAwards')
                                    <div class="d-flex justify-content-end mt-5">
                                        <input type="submit" class="btn btn-primary btn-active-danger mb-3" value="Enviar Prêmio"></input>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
</div>
<!--end::Content-->
@endsection