@extends('layouts.app')
    
@section('page-title', 'Dashboard')

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Row-->
        <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
            @if (Auth::user()->role_id == 1)
            <!--begin::Col-->
            <div class="col-md-3">
                <!--begin::Card widget 17-->
                <div class="card h-100 card-flush mb-5 mb-10">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <div class="card-title d-flex flex-column">
                            <!--begin::Info-->
                            <div class="d-flex align-items-center">
                                <!--begin::Currency-->
                                <span class="fs-4 fw-semibold text-gray-400 me-3 align-self-start">pts</span>
                                <!--end::Currency-->
                                <!--begin::Amount-->
                                <span class="fs-2hx fw-bold text-dark me-2 lh-1 ls-n2">{{ $points->sum('points') }}</span>
                                <!--end::Amount-->
                                <!--begin::Badge-->
                                <span class="badge badge-light-success fs-base"></span>
                                <!--end::Badge-->
                            </div>
                            <!--end::Info-->
                            <!--begin::Subtitle-->
                            <span class="text-gray-400 pt-1 fw-semibold fs-6">Total de pontos no sistema</span>
                            <!--end::Subtitle-->
                        </div>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-2 pb-8 d-flex flex-wrap align-items-center">
                        <!--begin::Chart-->
                        <div class="d-flex flex-center me-5 pt-2">
                            <div id="kt_card_widget_17_chart" style="min-width: 70px; min-height: 70px" data-kt-size="70" data-kt-line="11"></div>
                        </div>
                        <!--end::Chart-->
                        <!--begin::Labels-->
                        <div class="d-flex flex-column content-justify-center flex-row-fluid">
                            <!--begin::Label-->
                            <div class="d-flex fw-semibold align-items-center">
                                <!--begin::Bullet-->
                                <div class="bullet w-8px h-3px rounded-2 bg-success me-3"></div>
                                <!--end::Bullet-->
                                <!--begin::Label-->
                                <div class="text-gray-500 flex-grow-1 me-4">Usados</div>
                                <!--end::Label-->
                                <!--begin::Stats-->
                                <div class="fw-bolder text-gray-700 text-xxl-end">{{ -$points->whereNotNull('award_id')->sum('points') }}</div>
                                <!--end::Stats-->
                            </div>
                            <!--end::Label-->
                            <!--begin::Label-->
                            <div class="d-flex fw-semibold align-items-center my-3">
                                <!--begin::Bullet-->
                                <div class="bullet w-8px h-3px rounded-2 bg-primary me-3"></div>
                                <!--end::Bullet-->
                                <!--begin::Label-->
                                <div class="text-gray-500 flex-grow-1 me-4">Solicitados</div>
                                <!--end::Label-->
                                <!--begin::Stats-->
                                <div class="fw-bolder text-gray-700 text-xxl-end">250</div>
                                <!--end::Stats-->
                            </div>
                            <!--end::Label-->
                            <!--begin::Label-->
                            <div class="d-flex fw-semibold align-items-center">
                                <!--begin::Bullet-->
                                <div class="bullet w-8px h-3px rounded-2 me-3" style="background-color: #E4E6EF"></div>
                                <!--end::Bullet-->
                                <!--begin::Label-->
                                <div class="text-gray-500 flex-grow-1 me-4">Não usados</div>
                                <!--end::Label-->
                                <!--begin::Stats-->
                                <div class="fw-bolder text-gray-700 text-xxl-end">{{ $points->sum('points') + $points->whereNotNull('award_id')->sum('points') }}</div>
                                <!--end::Stats-->
                            </div>
                            <!--end::Label-->
                        </div>
                        <!--end::Labels-->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Card widget 17-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-md-3">
                <!--begin::List widget 26-->
                <div class="card h-100 card-flush">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h3 class="card-title text-gray-800 fw-bold">Links Úteis</h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-5">
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <a href="{{ route('users.create') }}" class="text-primary fw-semibold fs-6 me-2">Adicionar Usuário</a>
                            <!--end::Section-->
                            <!--begin::Action-->
                            <a href="{{ route('users.create') }}" class="btn btn-icon btn-sm h-auto btn-color-gray-400 btn-active-color-primary justify-content-end">
                                <i class="ki-duotone ki-exit-right-corner fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </a>
                            <!--end::Action-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-3"></div>
                        <!--end::Separator-->
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <a href="{{ route('awards.create') }}" class="text-primary fw-semibold fs-6 me-2">Adicionar Prêmio</a>
                            <!--end::Section-->
                            <!--begin::Action-->
                            <a href="{{ route('awards.create') }}" class="btn btn-icon btn-sm h-auto btn-color-gray-400 btn-active-color-primary justify-content-end">
                                <i class="ki-duotone ki-exit-right-corner fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </a>
                            <!--end::Action-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-3"></div>
                        <!--end::Separator-->
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <a href="{{ route('send.awards.create') }}" class="text-primary fw-semibold fs-6 me-2">Enviar Prêmio</a>
                            <!--end::Section-->
                            <!--begin::Action-->
                            <a href="{{ route('send.awards.create') }}" class="btn btn-icon btn-sm h-auto btn-color-gray-400 btn-active-color-primary justify-content-end">
                                <i class="ki-duotone ki-exit-right-corner fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </a>
                            <!--end::Action-->
                        </div>
                        <!--end::Item-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::LIst widget 26-->
            </div>
            <!--end::Col-->
            
            <!--begin::Col-->
            <div class="col-md-3">
                <!--begin::List widget 26-->
                <div class="card h-100 card-flush">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h3 class="card-title text-gray-800 fw-bold">Dados do Sistema</h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-5">
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-3"></div>
                        <!--end::Separator-->
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">Prêmios</div>
                            <!--end::Section-->
                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{ $awards->where('status', 1)->count() }}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-3"></div>
                        <!--end::Separator-->
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">Arquitetos</div>
                            <!--end::Section-->
                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{ $users->where('role_id', 2)->where('status', 1)->count() }}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-3"></div>
                        <!--end::Separator-->
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">Decoradores</div>
                            <!--end::Section-->
                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{ $users->where('role_id', 3)->where('status', 1)->count() }}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                        </div>
                        <!--end::Item-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::LIst widget 26-->
            </div>
            <!--end::Col-->
            
            <!--begin::Col-->
            <div class="col-md-3">
                <!--begin::List widget 26-->
                <div class="card h-100 card-flush">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h3 class="card-title text-gray-800 fw-bold">-</h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-5">
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-3"></div>
                        <!--end::Separator-->
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">Aniversáriantes</div>
                            <!--end::Section-->
                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{ $birthdays->count() }}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-3"></div>
                        <!--end::Separator-->
                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">Cadastros pendentes</div>
                            <!--end::Section-->
                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{ $users->where('status', 2)->count() }}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                        </div>
                        <!--end::Item-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::LIst widget 26-->
            </div>
            <!--end::Col-->
            <div class="col-md-12 mb-md-5 mb-xl-10">
                <!--begin::List widget 26-->
                <div class="card h-100 card-flush">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h3 class="card-title text-gray-800 fw-bold">Pontuação Atual</h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-5">
                        <table id="datatablesRanking" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                            <thead>
                                <tr class="fw-bold fs-6 text-gray-800 px-7">
                                    <th class="d-none"></th>
                                    <th>Pontos</th>
                                    <th>Nome</th>
                                    <th>Tipo</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody class="table-pd">
                                @foreach ($users as $key => $user)
                                <tr>
                                    <td class="d-none">
                                        @if($user->points->sum('points') != 0)
                                            {{ $user->points->sum('points') }}
                                        @endif
                                    </td>
                                    <td class="text-gray-700">
                                        @if($user->points->sum('points') != 0)
                                        <span class="badge @if($user->points->sum('points') < 0) badge-light-danger @else badge-light-success @endif">
                                            {{ $user->points->sum('points') }}
                                        @else
                                        <span class="badge badge-light">
                                           Sem pontos
                                        </span>
                                        @endif
                                    </td>
                                    <td style="min-width: 25%; padding: 11px;">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-30px me-5">
                                                <img src="{{ searchImage('usuarios', $user->id, true, true) }}" class="rounded object-fit-cover" alt="">
                                            </div>
                                            <div class="d-flex justify-content-start">
                                                <a href="{{ route('users.edit', $user->id) }}" class="text-gray-700 fw-bold text-hover-primary fs-6">{{ Str::limit($user->name, 20) }}</a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @if ($user->role_id == 1)
                                            <span class="badge badge-light-danger">Administrador</span>
                                        @elseif($user->role_id == 2)
                                            <span class="badge badge-light-info">Arquiteto</span>
                                        @else
                                            <span class="badge badge-light-primary">Decorador</span>
                                        @endif
                                    </td>
                                    <td class="text-gray-700">
                                        <div class="d-flex align-items-center">
                                            @if($user->status == 1)
                                            <span class="badge badge-light-primary">Ativo</span>
                                            @elseif($user->status == 2)
                                            <span class="badge badge-light-warning">Pendente</span>
                                            @else
                                            <span class="badge badge-light-danger">Inativo</span>
                                            @endif
                                        </div>
                                    </td>
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
            <div class="card">
                <div class="card-body">
                    <table id="datatables" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800 px-7">
                                <th class="w-50px">ID</th>
                                <th>Solicitou Prêmio</th>
                                <th>Na data:</th>
                                <th>Atualizado por</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody class="table-pd">
                            @foreach (Auth::user()->requestAwards as $content)
                            <tr>
                                <td>
                                    <span class="text-gray-700">{{ $content->id }}</span>
                                </td>
                                <td class="text-gray-700">
                                    <span class="badge badge-light-info fs-7">{{ $content->award->name }}</span>
                                </td>
                                <td>
                                    <span class="badge badge-light">{{ $content->award->created_at->format('d/m/Y H:i:s') }}</span>
                                </td>
                                <td>
                                    @if (isset($content->approved->name))
                                    <span class="badge badge-primary">{{ Str::limit($content->approved->name, 20) }}</span>        
                                    @else
                                    <span class="badge badge-light">Sem alteração</span>
                                    @endif
                                </td>
                                <td class="text-gray-700">
                                    <div class="d-flex align-items-center">
                                        @if($content->status == 1)
                                        <span class="badge badge-light-success">Aprovado</span>
                                        @elseif($content->status == 3)
                                        <span class="badge badge-light-danger">Negado</span>
                                        @else
                                        <span class="badge badge-light-warning">Solicitado</span>
                                        @endif
                                    </div>
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
        </div>
        <!--end::Row-->
    </div>
    <!--end::Content container-->
</div>
@endsection

@section('custom-footer')
<script>
    loadTables('#datatablesRanking', 25, [0, 'desc']);
</script>
@endsection