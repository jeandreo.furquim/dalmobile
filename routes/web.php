<?php

use App\Http\Controllers\AwardsController;
use App\Http\Controllers\PointsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SendAwardsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\VounchersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('auth')->group(function () {

    Route::get('/', [UsersController::class, 'dashboard'])->name('dashboard');

    // USERS
    Route::middleware('admin')->prefix('usuarios')->group(function () {
        Route::name('users.')->group(function () {
            Route::get('/adicionar', [UsersController::class, 'create'])->name('create');
            Route::post('/adicionar', [UsersController::class, 'store'])->name('store');
            Route::get('/visualizando/{id}', [UsersController::class, 'show'])->name('show');
            Route::get('/desabilitar/{id}', [UsersController::class, 'destroy'])->name('destroy');
            Route::get('/editar/{id}', [UsersController::class, 'edit'])->name('edit');
            Route::put('/editar/{id}', [UsersController::class, 'update'])->name('update');
            Route::get('/minha-conta', [UsersController::class, 'account'])->name('account');
            Route::put('/minha-conta', [UsersController::class, 'accountUp'])->name('accountUp');
            Route::get('/barra-lateral', [UsersController::class, 'aside'])->name('aside');
            Route::get('/aniversariantes', [UsersController::class, 'birthdays'])->name('birthdays');
            Route::get('/aprovar/{id}/{status}', [UsersController::class, 'aprove'])->name('aprove');
            Route::get('/ranking', [UsersController::class, 'ranking'])->name('ranking');
            Route::get('/{id?}', [UsersController::class, 'index'])->name('index');
        });
    });
    
    // USERS
    Route::prefix('pontos')->group(function () {
        Route::name('points.')->group(function () {
            Route::get('/', [PointsController::class, 'index'])->name('index');
            Route::post('/adicionar', [PointsController::class, 'store'])->name('store')->middleware('admin');
            Route::get('/pontuacao', [PointsController::class, 'table'])->name('table');
        });
    });
    
    // USERS
    Route::prefix('premios')->middleware('admin')->group(function () {
        Route::name('awards.')->group(function () {
            Route::get('/', [AwardsController::class, 'index'])->name('index');
            Route::get('/adicionar', [AwardsController::class, 'create'])->name('create');
            Route::post('/adicionar', [AwardsController::class, 'store'])->name('store');
            Route::get('/visualizando/{id}', [AwardsController::class, 'show'])->name('show');
            Route::get('/desabilitar/{id}', [AwardsController::class, 'destroy'])->name('destroy');
            Route::get('/editar/{id}', [AwardsController::class, 'edit'])->name('edit');
            Route::put('/editar/{id}', [AwardsController::class, 'update'])->name('update');
        });
    });
    
    // USERS
    Route::prefix('envio-de-premios')->middleware('admin')->group(function () {
        Route::name('send.awards.')->group(function () {
            Route::get('/', [SendAwardsController::class, 'index'])->name('index');
            Route::get('/adicionar', [SendAwardsController::class, 'create'])->name('create');
            Route::post('/adicionar', [SendAwardsController::class, 'store'])->name('store');
            Route::get('/visualizando/{id}', [SendAwardsController::class, 'show'])->name('show');
            Route::get('/desabilitar/{id}/{status}', [SendAwardsController::class, 'destroy'])->name('destroy');
            Route::get('/editar/{id}', [SendAwardsController::class, 'edit'])->name('edit');
            Route::put('/editar/{id}', [SendAwardsController::class, 'update'])->name('update');
            Route::get('/email', [SendAwardsController::class, 'mail'])->name('email');
        });
    });
    
    Route::get('/solicitar', [AwardsController::class, 'results'])->name('awards.results');
    Route::get('/solicitar/{id}', [SendAwardsController::class, 'request'])->name('send.awards.request');

});

Route::get('/teste-email/{email?}', [SendAwardsController::class, 'mail'])->name('email');


require __DIR__.'/auth.php';
