<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SendAward extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'award_id',
        'status',
        'observation',
        'created_by',
        'updated_by',
    ];
    

    /**
    * Get the comments for the blog post.
    */
   public function author(): HasOne
   {
       return $this->hasOne(user::class, 'id', 'created_by');
   }

   /**
   * Get the comments for the blog post.
   */
  public function sendFor(): HasOne
  {
      return $this->hasOne(user::class, 'id', 'user_id');
  }

  /**
  * Get the comments for the blog post.
  */
 public function award(): HasOne
 {
     return $this->hasOne(Award::class, 'id', 'award_id');
 }

   /**
  * Get the comments for the blog post.
  */
  public function approved(): HasOne
  {
    return $this->hasOne(user::class, 'id', 'updated_by');
  }
}