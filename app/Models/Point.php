<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Point extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'title',
        'points',
        'award_id',
        'contract',
        'observations',
        'created_by',
    ];

     /**
     * Get the comments for the blog post.
     */
    public function author(): HasOne
    {
        return $this->hasOne(user::class, 'id', 'created_by');
    }
}
