<?php

use App\Models\Alert;
use App\Models\CategoryMessage;
use App\Models\LinkPersonalization;
use App\Models\PrinterModel;
use App\Models\Project;
use App\Models\ProjectHistoric;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


// HELLOU
function greeting(){

    // SET GREETING BASED IN HOUR
    $hour = date('H');
    if ($hour < 12) {
        $greeting = "Bom dia 😁";
    } else if ($hour < 18) {
        $greeting = "Boa tarde 😁";
    } else {
        $greeting = "Boa noite 😁";
    }
    return $greeting;
}


// VERIFY IF THE USER HAS A PROFILE IMAGE
function searchImage($path, $id, $defaultImage = 'default', $cache = true) {

    // DEFAULT IMAGE
    if($defaultImage == 'default'){
        $defaultImage = url("assets/media/images/blank.png");
    }

    // CACHE
    $randow = '?' . rand(0, 999);

    // VERIFY IF IMAGE EXIST
    $existImage = Storage::disk('local')->exists("public/$path/$id.jpg");

    $url = $existImage == true ? url("storage/$path/$id.jpg") . $randow : $defaultImage;
    
    return $url;

}

function storeImage($path, $image, $name){

    if ($image != null && $image->isValid()) {
        // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW
        if (Storage::exists($path . $name . '.jpg')) {
            Storage::delete($path . $name . '.jpg');
        }
        $image->storeAs($path, $name . '.jpg');
    }

}


// PUT THE BACKGROUND IN THE TEXT COLOR
function hex2rgb($colour, $opacity) {
    
    // REMOVE # FROM STRING
    $colour = ltrim($colour, '#');

    // EXTRACT RGB FROM HEX
    $rgb = sscanf($colour, '%2x%2x%2x');
    $rgb[] = $opacity;

    // RETURN RGBA
    return sprintf('rgb(%d, %d, %d, %d%%)', ...$rgb);

}

// CONVERT DATA PT TO US
function convertDateFormat($date)
{
    $dateObj = date_create_from_format('d/m/Y', $date);
    return date_format($dateObj, 'Y-m-d');
}

function getAge($birth){
    $age = Carbon::parse($birth)->age;
    return $age;
}