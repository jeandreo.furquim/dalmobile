<?php

namespace App\Http\Controllers;

use App\Mail\Notifications;
use App\Models\Award;
use App\Models\Point;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, User $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {

        // GET ALL DATA
        $users = User::orderBy('name', 'ASC')->get();
        $awards = Award::get();
        $points = Point::get();
        $currentMonth = Carbon::now()->month;
        $birthdays = User::whereMonth('birth', $currentMonth)
                        ->orderBy('name', 'ASC')
                        ->get();

        // RETURN VIEW WITH DATA
        return view('dashboard', [
            'users' => $users,
            'awards' => $awards,
            'points' => $points,
            'birthdays' => $birthdays,
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {

        if($id != null){
            $contents = User::where('role_id', $id)->orderBy('name', 'ASC')->get();
        } else {
            $contents = User::orderBy('name', 'ASC')->get();
        }

        // GET ALL DATA

        // RETURN VIEW WITH DATA
        return view('pages.users.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ranking()
    {

        // GET ALL DATA
        $contents = User::orderBy('name', 'ASC')->get();

        // RETURN VIEW WITH DATA
        return view('pages.users.ranking', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // GET ALL DATA
        return view('pages.users.create');
    }

    // DEFINE FOLDER ROUTE
    public function folderRoute()
    {
        return "public/usuarios/";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // VERIFY IF TITLE IS UNIQUE
        $emailExist = User::where('email', $request->email)->count();

        if($emailExist > 0){
            if(!Auth::check() || Auth::user()->role_id != 1){
                return redirect()->route('register.email', $request->email );
            } else {
                // REDIRECT AND MESSAGES
                return redirect()
                 ->route('users.create')
                 ->with('message', 'Já existe um usuário com esse email.');
            }
        }

        // GET FORM DATA
        $data = $request->all();

        // CREATED BY
        $data['password'] = Hash::make($request->password);
        $data['created_by'] = Auth::id();

        if(!Auth::check() || Auth::user()->role_id != 1){
            $data['role_id'] = 2;
            $data['status'] = 2;
        }
        
        $data['birth'] = isset($data['birth']) ? convertDateFormat($data['birth']) : null; 

        // SEND DATA
        $insert = $this->repository->create($data);

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE
        storeImage($this->folderRoute(), $request->image, $insert->id);

        // REDIRECT AND MESSAGES
        if(!Auth::check() || Auth::user()->role_id != 1){
            return redirect()->route('register.thanks', $request->name);
        } else {
            return redirect()
                ->route('users.index')
                ->with('message', 'Usuário criado com sucesso.');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // VERIFY IF EXISTS
        if(!$content = User::find($id)){
            return redirect()->back();
        }

        // GENERATES DISPLAY WITH DATA
        return view('pages.users.edit')->with([
            'content' => $content,  
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // VERIFY IF TITLE IS UNIQUE
        $emailExist = User::where('email', $request->email)->where('id', '<>', $id)->count();

        if($emailExist > 0){
             // REDIRECT AND MESSAGES
                return redirect()
                ->route('users.edit', $id)
                ->with('message', 'Já existe um usuário com esse email.');
        }

        // GET FORM DATA
        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE
        storeImage($this->folderRoute(), $request->image, $id);

        // VERIFY IF PASSWORD
            if ($data['password'] != '') {
                $data['password'] = Hash::make($request->password);
            } else {
                unset($data['password']);
            }
        
        // UPDATE BY
        $data['updated_by'] = Auth::id();
        $data['birth'] = isset($data['birth']) ? convertDateFormat($data['birth']) : null; 

        // STORING NEW DATA
        $content = User::find($id);
        $content->update($data);

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('users.edit', $id)
        ->with('message', 'Usuário atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        // GET DATA
        $content = User::find($id);

        // STORING NEW DATA
        if($content->status == 1){
            User::where('id', $id)->update(['status' => 0]);
            $message = 'desabilitado';
        } else {
            User::where('id', $id)->update(['status' => 1]);
            $message = 'habilitado';
        }
        

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('users.index')
        ->with('message', 'Usuário <b>'. $content->name . '</b> '. $message .' com sucesso.');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function account()
    {
        // VERIFY IF EXISTS
        if(!$content = User::find(Auth::id())){
            return redirect()->back();
        }

        // GENERATES DISPLAY WITH DATA
        return view('pages.users.account')->with([
            'content' => $content,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accountUp(Request $request)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find(Auth::id()))
        return redirect()->back();

        // GET FORM DATA
        $data = $request->all();

        // VERIFY IF PASSWORD
        if($data['password'] != ''){
            // STORING NEW DATA
            User::where('id', Auth::id())->update([
                'password' => Hash::make($request->password),
            ]);
        } else {
            unset($data['password']);
        }

        // UPDATE BY
        $data['updated_by'] = Auth::id();

        // UPDATE ACCOUNT
        $content->update($data);

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE
        storeImage($this->folderRoute(), $request->image, $content->id);

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('users.account')
        ->with('message', 'Conta atualizada com sucesso.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function birthdays()
    {

        // GET ALL DATA
        $currentMonth = Carbon::now()->month;
        $contents = User::whereMonth('birth', $currentMonth)
                        ->orderBy('name', 'ASC')
                        ->get();
                        
        // RETURN VIEW WITH DATA
        return view('pages.users.birthdays', [
            'contents' => $contents,
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function thanks($name)
    {
        // RETURN VIEW WITH DATA
        return view('auth.thank')->with('name', $name);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function email($email)
    {
        // RETURN VIEW WITH DATA
        return view('auth.email')->with('email', $email);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aprove($id, $status)
    {
        
        // GET DATA
        $content = User::find($id);
        $content->status = $status;

        if($content->save()){

            // EMAIL
            $email = $content->email;

            // PARAMETERS FROM EMAIL
            $parameters = [
                'fromName' => 'Dalmóbile',
                'fromEmail' => 'smtp@dreamake.com.br',
                'subject' => 'Você foi aprovado na Dalmobilesjc!',
            ];

            // CREATE URL FOR RESET PASSWORD
            $btn = 'ACESSAR MINHA CONTA';
            $url = route('dashboard');

            // MESSAGE
            $message = 'É com grande satisfação que lhe damos as boas-vindas à nossa plataforma. Estamos empolgados por tê-lo(a) a bordo e esperamos que esta jornada conosco seja produtiva e recompensadora.';
            
            // CONTENT OF EMAIL
            $data = [
                'name' => $content->name,
                'email' => $email,
                'url' => $url,
                'btn' => $btn,
                'message' => $message,
            ];
            
            // SEND EMAIL
            Mail::to($email)->send(new Notifications($data, $parameters['subject'], $parameters['fromEmail'], $parameters['fromName']));
        }

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('users.index')
        ->with('message', 'Status do usuário <b>'. $content->name . '</b> atualizado com sucesso.');

    }
    

}