<?php

namespace App\Http\Controllers;

use App\Mail\Notifications;
use App\Models\Award;
use App\Models\Point;
use App\Models\SendAward;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendAwardsController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, SendAward $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = SendAward::get();

        // RETURN VIEW WITH DATA
        return view('pages.send-awards.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // GET ALL DATA
        $awards = Award::where('status', 1)->get();
        $users = User::where('status', 1)->where('role_id', 2)->get();
        return view('pages.send-awards.create')->with([
            'awards' => $awards,
            'users' => $users,
        ]);
    }

    // DEFINE FOLDER ROUTE
    public function folderRoute()
    {
        return "public/envio-de-premios/";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA
        $data = $request->all();

        // CREATED BY
        $data['created_by'] = Auth::id();

        // GET TOTAL POINTS USER
        $pointsUser = Point::where('user_id', $request->user_id)->sum('points');

        // GET AWARD
        $award = Award::find($request->award_id);

        // IF USER HAVE POINTS REQUIRED
        if($pointsUser < $award->points) {
            // REDIRECT AND MESSAGES
            return redirect()
                ->route('send.awards.index')
                ->with('message', 'O usuário não possui pontos suficientes para este prêmio.');
        }

        Point::create([
            'user_id' => $request->user_id,
            'title' => 'Prêmio resgatado: ' . $award->name,
            'points' => -$award->points,
            'award_id' => $award->id,
            'created_by' => Auth::id(),
        ]);

        // SEND DATA
        $send = $this->repository->create($data);

        // GET USER
        $user = User::find($request->user_id);

        if($send){

            // EMAIL
            $email = $user->email;

            // PARAMETERS FROM EMAIL
            $parameters = [
                'fromName' => 'Dalmóbile',
                'fromEmail' => 'smtp@dreamake.com.br',
                'subject' => 'Você acabou de receber um Prêmio!',
            ];

            // CREATE URL FOR RESET PASSWORD
            $btn = 'ACESSAR MINHA CONTA';
            $url = route('dashboard');

            // MESSAGE
            $message = $request->observation;
            
            // CONTENT OF EMAIL
            $data = [
                'name' => $user->name,
                'email' => $email,
                'url' => $url,
                'btn' => $btn,
                'message' => $message,
            ];
            
            // SEND EMAIL
            Mail::to($email)->send(new Notifications($data, $parameters['subject'], $parameters['fromEmail'], $parameters['fromName']));
        }

        // REDIRECT AND MESSAGES
        return redirect()
            ->route('send.awards.index')
            ->with('message', 'Prêmio criado com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // VERIFY IF EXISTS
        if(!$content = SendAward::find($id)){
            return redirect()->back();
        }

        // GENERATES DISPLAY WITH DATA
        return view('pages.send-awards.edit')->with([
            'content' => $content,  
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // GET FORM DATA
        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE
        storeImage($this->folderRoute(), $request->image, $id);

        // UPDATE BY
        $data['updated_by'] = Auth::id();

        // STORING NEW DATA
        $content = SendAward::find($id);
        $content->update($data);

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('send.awards.index')
        ->with('message', 'Prêmio atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $status)
    {
        
        // GET DATA
        $content = SendAward::find($id);

        // STORING NEW DATA
        if($status == 1){
            SendAward::where('id', $id)->update(['status' => $status, 'updated_by' => Auth::id()]);
            $message = 'aprovado';
            
            Point::create([
                'user_id' => $content->user_id,
                'title' => 'Prêmio resgatado: ' . $content->award->name,
                'points' => -$content->award->points,
                'award_id' => $content->award->id,
                'created_by' => Auth::id(),
            ]);

        } else {
            SendAward::where('id', $id)->update(['status' => $status, 'updated_by' => Auth::id()]);
            $message = 'cancelado';
        }

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('send.awards.index')
        ->with('message', 'Prêmio <b>'. $content->name . '</b> '. $message .' com sucesso.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function request(Request $request, $id)
    {

        // GET FORM DATA
        $data = $request->all();

        // CREATED BY
        $data['created_by'] = Auth::id();

        // GET TOTAL POINTS USER
        $pointsUser = Point::where('user_id', Auth::id())->sum('points');

        // GET AWARD
        $award = Award::find($id);
        

        if($award->valid < date('Y-m-d')){
            // REDIRECT AND MESSAGES
            return redirect()
                ->route('dashboard')
                ->with('message', 'O Prêmio já não é mais válido.');
        }

        // IF USER HAVE POINTS REQUIRED
        if($pointsUser < $award->points) {
            // REDIRECT AND MESSAGES
            return redirect()
                ->route('dashboard')
                ->with('message', 'Você não possui pontos suficientes.');
        }

        // SEND DATA
        SendAward::create([
            'user_id' => Auth::id(),
            'award_id' => $id,
            'status' => 2,
            'created_by' => Auth::id(),
        ]);

        // REDIRECT AND MESSAGES
        return redirect()
            ->route('dashboard')
            ->with('message', 'Prêmio solicitado com sucesso.');
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mail($email = 'jeandreofur@gmail.com')
    {

        // PARAMETERS FROM EMAIL
        $parameters = [
            'fromName' => 'Dalmóbile',
            'fromEmail' => 'smtp@dreamake.com.br',
            'subject' => 'Você acabou de receber um Prêmio!',
        ];

        // CREATE URL FOR RESET PASSWORD
        $btn = 'ACESSAR MINHA CONTA';
        $url = route('dashboard');

        // MESSAGE
        $message = 'Você está recebendo este e-mail porque fez uma solicitação de redefinição de senha para sua conta no site da Dalmobilesjc.';
        
        // CONTENT OF EMAIL
        $data = [
            'name' => 'Jeandreo',
            'email' => $email,
            'url' => $url,
            'btn' => $btn,
            'message' => $message,
        ];
        
        // SEND EMAIL
        Mail::to($email)->send(new Notifications($data, $parameters['subject'], $parameters['fromEmail'], $parameters['fromName']));
        
    }

}