<?php

namespace App\Http\Controllers;

use App\Mail\Notifications;
use App\Models\Point;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PointsController extends Controller
{
   
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Point $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // GET ALL DATA
        $users = User::where('status', 1)->where('role_id', 2)->get();
        return view('pages.points.index')->with([
            'users' => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA
        $data = $request->all();

        // CREATED BY
        $data['created_by'] = Auth::id();

        // TITLE
        $data['title'] = 'Pontos adicionados';

        if($request->addOrRemove == 2){
            $data['points'] = -$data['points'];
            $data['title'] = 'Pontos removidos';
        }

        // SEND DATA
        $update = $this->repository->create($data);

        // GET USER
        $user = User::find($request->user_id);

        if($update){

            // EMAIL
            $email = $user->email;

            // PARAMETERS FROM EMAIL
            $parameters = [
                'fromName' => 'Dalmóbile',
                'fromEmail' => 'smtp@dreamake.com.br',
                'subject' => 'Pontos recebidos na plataforma!',
            ];

            // CREATE URL FOR RESET PASSWORD
            $btn = 'ACESSAR MINHA CONTA';
            $url = route('dashboard');

            // MESSAGE
            $message = 'Parabéns você acaba de receber ' . $data['points'] . ' pontos em sua conta.';
            
            // CONTENT OF EMAIL
            $data = [
                'name' => $user->name,
                'email' => $email,
                'url' => $url,
                'btn' => $btn,
                'message' => $message,
            ];
            
            // SEND EMAIL
            Mail::to($email)->send(new Notifications($data, $parameters['subject'], $parameters['fromEmail'], $parameters['fromName']));
        }

        // REDIRECT AND MESSAGES
        return redirect()
            ->route('points.index')
            ->with('message', 'Pontos adicionados com sucesso.');
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function table()
    {
        // GET ALL DATA
        $users = User::where('status', 1)->where('role_id', 2)->get();
        return view('pages.points.table');
    }

}
