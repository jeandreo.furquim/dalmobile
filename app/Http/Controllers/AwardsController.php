<?php

namespace App\Http\Controllers;

use App\Mail\Notifications;
use App\Models\Award;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class AwardsController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Award $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = Award::orderBy('name', 'ASC')->get();

        // RETURN VIEW WITH DATA
        return view('pages.awards.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results()
    {

        // GET ALL DATA
        $contents = Award::where("valid", '>=', date('Y-m-d'))->orWhere('valid', null)->orderBy('name', 'ASC')->get();

        // RETURN VIEW WITH DATA
        return view('pages.awards.results', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // GET ALL DATA
        return view('pages.awards.create');
    }

    // DEFINE FOLDER ROUTE
    public function folderRoute()
    {
        return "public/premios/";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA
        $data = $request->all();

        // CREATED BY
        $data['created_by'] = Auth::id();
        $data['valid'] = $data['valid'] != null ? convertDateFormat($data['valid']) : null;

        // SEND DATA
        $insert = $this->repository->create($data);

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE
        storeImage($this->folderRoute(), $request->image, $insert->id);

        if($insert){

            // PARAMETERS FROM EMAIL
            $parameters = [
                'fromName' => 'Dalmóbile',
                'fromEmail' => 'smtp@dreamake.com.br',
                'subject' => 'Novo prêmio adicionado a plataforma!',
            ];

            // CREATE URL FOR RESET PASSWORD
            $btn = 'ACESSAR MINHA CONTA';
            $url = route('dashboard');

            // MESSAGE
            $message = 'Acabamos de adicionar ' . $request->name . ', veja em sua conta quantos pontos você possui, se tiver pontos suficiente e desejar, restage o prêmio!';
            
            // GET USERS
            $users = User::where('status', 1)->get();

            foreach($users as $user){
                // EMAIL
                $email = $user->email;

                // CONTENT OF EMAIL
                $data = [
                    'name' => $user->name,
                    'email' => $email,
                    'url' => $url,
                    'btn' => $btn,
                    'message' => $message,
                ];
                
                try {
                    Mail::to($email)->send(new Notifications($data, $parameters['subject'], $parameters['fromEmail'], $parameters['fromName']));
                } catch (\Exception $e) {
                    // Captura e registra a exceção, permitindo que o loop continue
                    // Isso NÃO é recomendado na maioria das situações
                    Log::error('Erro ao enviar email para ' . $email . ': ' . $e->getMessage());
                }
            }
        }

        // REDIRECT AND MESSAGES
        return redirect()
            ->route('awards.index')
            ->with('message', 'Prêmio criado com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // VERIFY IF EXISTS
        if(!$content = Award::find($id)){
            return redirect()->back();
        }

        // GENERATES DISPLAY WITH DATA
        return view('pages.awards.edit')->with([
            'content' => $content,  
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // GET FORM DATA
        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE
        storeImage($this->folderRoute(), $request->image, $id);

        // UPDATE BY
        $data['updated_by'] = Auth::id();
        $data['valid'] = $data['valid'] != null ? convertDateFormat($data['valid']) : null;

        // STORING NEW DATA
        $content = Award::find($id);
        $content->update($data);

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('awards.index')
        ->with('message', 'Prêmio atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        // GET DATA
        $content = Award::find($id);

        // STORING NEW DATA
        if($content->status == 1){
            Award::where('id', $id)->update(['status' => 0]);
            $message = 'desabilitado';
        } else {
            Award::where('id', $id)->update(['status' => 1]);
            $message = 'habilitado';
        }
        

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('awards.index')
        ->with('message', 'Prêmio <b>'. $content->name . '</b> '. $message .' com sucesso.');

    }

}