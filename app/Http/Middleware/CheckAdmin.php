<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // VERIFY LOGGED AND ADMIN USER
        if (auth()->check() && auth()->user()->role_id == 1) {
            return $next($request);
        }

        // REDIRECT
        return redirect()
            ->route('dashboard')
            ->with('message', 'Você não tem permissão!');
    }
}
